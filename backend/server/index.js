const express = require("express");
const PORT = process.env.PORT || 3003
const app = express();
const cors = require("cors");
const db = require('./queries');
const ur = require('./user');
const rateLimit = require("express-rate-limit");
const { response } = require("express");

app.use(express.json());

app.use((req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin', 'http://localhost:3000');
    res.setHeader('Access-Control-Allow-Methods', 'GET,POST,PUT,DELETE,OPTIONS');
    res.setHeader('Access-Control-Allow-Headers', 'Content-Type, Access-Control-Allow-Headers');
    next();
});

// whitelist
const whitelist = ['http://localhost:3000', 'http://localhost:3001']
const corsOptions = {
    origin: function (origin, callback) {
        if (whitelist.indexOf(origin) !== -1) {
            callback(null, true)
        } else {
            callback(new Error('Not allowed by CORS'))
        }
    }
}

const limiter = rateLimit({
    windowMs: 10 * 60 * 1000, // 10 minutes
    max: 100, //limit each IP to 100 req per windowMs
    message: "Too many accounts created from this IP, please try again after 10 minute"
})

//Middlewares
app.use(cors(corsOptions));
app.use(limiter);

//Error-handling middleware
const errorhandling = (err, req, res, next) => {
    console.error(err.stack)
    res.status(500).send('Something broke!')
}
app.use(errorhandling)

//unknown endpoint
// const unknownEndpoint = (_req, res) => {
//     res.status(404).send({ error: "No one here" })
// }
// app.use(unknownEndpoint);

//error handler
const errorHandler = (error, req, res, next) => {
    logger.error(error.message)
    if (error.name === "SomeUserError") {
        res.status(400).send(error.message)
    }
    next(error)
};
app.use(errorHandler);

//PETS
app.get('/pets', (req, res) => {
    db.getPets()
        .then(response => {
            res.status(200).send(response);
        })
        .catch(error => {
            res.status(500).send(error);
        })
});

// getPetById,

app.post('/pets', (req, res) => {
    db.createPet(req.body);
    console.log('Pet is sended to Database:', req.body);
})

app.put('/pets/:id', (req, res) => {
    db.updatePet(req.body);
    console.log(req.body, req.params.id, 'update pet data');
})

app.delete('/pets/:id', (req, res) => {
    db.deletePet(req.params.id)
        .then(response => {
            res.status(200).send(response);
        })
        .then(console.log('Delete pet id ', req.params.id))
        .catch(err => {
            res.status(500).send(err);
        })
})

//USER
app.use("/login", (req, res) => {

    ur.login(req.body)
        .then(response => {
            res.send(response);
            console.log(response);
        })
        .catch(error => {
            res.status(500).send(error);
        })
})

app.post("/register", (req, res) => {
    ur.register(req.body);
    console.log('New User is adedd: ', req.body);
})

//////////////////
let pets = [];
//get
app.get("/petsList", (req, res) => {
    res.json(pets);
});

//post
app.post("/pet", (req, res) => {
    const data = req.body;

    console.log("Pet is added to local server");
    pets.push(data);
    console.log(pets, "array pets");
})

//delete
app.delete("/petsList/:id", (req, res) => {
    const id = req.params.id;

    pets.splice(id, 1);
})

//put-edit
app.put("/petsList/:id", (req, res) => {
    const id = req.params.id;
    const data = req.body;

    pets.splice(id, 1, data);
})


//return pet by id/index
//app.get("/petsList/:id"  ...)
/////////////////////////////////
app.get("/api", (req, res) => {
    res.json({ message: "Here is your server!" });
});

app.listen(PORT, () => {
    console.log(`Server listening on . ${PORT}`);
});