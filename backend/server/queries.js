const { request } = require('express');

const dbConnection = require('./pool');


const getPets = () => {
    return new Promise((resolve, reject) => {
        dbConnection.pool.query('SELECT * FROM pets', (error, results) => {// ORDER BY id ASC
            if (error) {
                reject(error)
            }
            resolve(results.rows);
            // console.log(results.rows);
        })
    })
};

const createPet = (body) => {

    return new Promise((resolve, reject) => {
        const name = body.name;
        const petType = body.petType;
        const birthday = body.birthday;
        const weight = body.weight;
        const image = body.image; //add to query
        const backLength = body.backLength;
        const neckGirth = body.neckGirth;
        const chestGirth = body.chestGirth;
        const feedName = body.feedName;
        const feedWeightPckg = body.feedWeightPckg;
        const feedPrice = body.feedPrice;
        const feedImage = null;//add to query
        const moreInfo = body.moreInfo;
        const userId = body.userId;

        console.log(name, petType, birthday, weight, "test from queries")

        // dbConnection.pool.query(
        //     'INSERT INTO pets (name, pet_type, birthday, weight, back_length, neck_girth, chest_girth, feed_name, feed_weight_pckg, feed_price, more_info, user_id) VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12) RETURNING *',
        //     [name, petType, '2003-12-12', weight, backLength, neckGirth, chestGirth, feedName, feedWeightPckg, feedPrice, moreInfo, 8],
        //     (error, results) => {
        //         if (error) {
        //             reject(error)
        //         }
        //         // resolve(`A new pet has been added : ${results.rows[0]}`)
        //         console.log(results, "results from queries post")
        //     })

        dbConnection.pool.query(
            'INSERT INTO pets (name, pet_type, birthday, weight, feed_name, user_id) VALUES ($1, $2, $3, $4, $5, $6, $7 ) RETURNING * ',
            [name, petType, '2003-10-10', weight, feedName, 8],
            (error, results) => {
                if (error) {
                    reject(error)
                }
                resolve(`A new pet has been added : ${results.rows[0]}`)
                console.log(results.rows, "TEST results from queries post")
            })
    })
}

const updatePet = (body) => {

    return new Promise((resolve, reject) => {
        const id = body.id;
        const name = body.name;
        const petType = body.petType;
        const birthday = body.birthday;
        const weight = body.weight;
        const image = body.image;
        const backLength = body.backLength;
        const neckGirth = body.neckGirth;
        const chestGirth = body.chestGirth;
        const feedName = body.feedName;
        const feedWeightPckg = body.feedWeightPckg;
        const feedPrice = body.feedPrice;
        const feedImage = body.feedImage;
        const moreInfo = body.moreInfo;
        const userId = body.userId;

        dbConnection.pool.query(
            'UPDATE pets SET name = $1, pet_type = $2, birthday = $3, weight = $4, image = $5, back_leight = $6, neck_girth = $7, chest_girth = $8, feed_name = $9, feed_weight_pckg = $10, feed_price = $11, feed_image = $12, more_info = $13, user_id = $14',
            [name, petType, birthday, weight, image, backLength, neckGirth, chestGirth, feedName, feedWeightPckg, feedPrice, feedImage, moreInfo, userId],
            (error, results) => {
                if (error) {
                    reject(error)
                }
                resolve(`Pet modified with ID: ${id}`)
            }
        )
    })
}

const deletePet = (id) => {

    return new Promise((resolve, reject) => {
        dbConnection.pool(
            'DELETE FROM pets WHERE id = $1',
            [id],
            (error, results) => {
                if (error) {
                    reject(error)
                }
                resolve(`Pet deleted with ID: ${id}`)
            }
        )
    })
}


module.exports = {
    getPets,
    // getPetById,
    createPet,
    updatePet,
    deletePet,
};