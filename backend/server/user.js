require('dotenv').config();
const { request } = require('express');
const jsonwebtoken = require('jsonwebtoken');
const bcrypt = require("bcryptjs");
const saltRounds = 10;
const dbConnection = require('./pool');

const JWT_SECRET = process.env.REACT_APP_JWT_SECRET;



const register = (body) => {

  return new Promise((resolve, reject) => {

    bcrypt.hash(body.password, saltRounds, (err, hash) => {

      const username = body.username;

      dbConnection.pool.query('INSERT INTO users (username, password) VALUES ($1, $2) RETURNING *',
        [username, hash],
        (err, results) => {
          if (err) {
            reject(err)
          }
          resolve(`new user ${username} has been added`)
        })
    })
  })

}

const login = (req, res) => {

  return new Promise((resolve, reject) => {
    const username = req.username;
    const password = req.password;

    dbConnection.pool.query('SELECT * FROM users WHERE username = $1', [username],
      (error, results) => {
        if (error) {
          reject(error)
        }

        if (results.rowCount != 0) {
          const aut = bcrypt.compare(password, results.rows[0].password)

          if (aut) {
            resolve({ token: jsonwebtoken.sign({ user: username }, JWT_SECRET) })//process.env.
            console.log("its true")
          } else {
            console.log("something is wrong");
          }
        }
      })
  })

}


module.exports = {
  login,
  register
};