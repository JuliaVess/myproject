import React from "react";
import "./css/mystyles.css";
import { Box, Container, Button } from "@mui/material";

import { LoginForm } from "./components/LoginForm";
import { LoggedIn } from "./components/LoggedIn";
import useToken from "./components/useToken";
import { BrowserRouter, Route, Routes } from "react-router-dom";



function App() {

  const { token, setToken } = useToken();

  if (!token) {
    return <LoginForm setToken={setToken} />;
  }


  return (
    <Container
      sx={{
        padding: "20px",
        backgroundColor: "rgb(180, 192, 180)",
        minHeight: "850px",
        textAlign: "center",
      }}>

      <Button color="secondary" onClick={() => setToken(false)} >LogOut</Button>

      <BrowserRouter>
        <Routes>
          <Route path="/" element={<LoggedIn />}>
          </Route>
        </Routes>
      </BrowserRouter>

      <Box sx={{
        position: "static",
        // marginBottom: "20px",
        bottom: "5px",
        paddingTop: "10px",
        marginTop: "200px"
      }}>
        <section className="section">
          <div className="container">
            <h1 className="title"> Hello World and Users </h1>
            <p className="subtitle"> React app  with <p />
              <strong> © Julia Vesselova 2022</strong> </p>
          </div>
        </section>
      </Box>
    </Container>

  );
}

export default App;