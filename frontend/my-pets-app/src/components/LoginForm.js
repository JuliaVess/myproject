import React from "react";
import { useState, useEffect } from "react";
import { Typography, Button } from "@mui/material";
import TextFieldInput from "./mui/TextFieldInput";
import ButtonAll from "./mui/ButtonAll";
import PropTypes from "prop-types";
import axios from "axios";


async function loginUser(credentials) {

  return fetch("http://localhost:3003/login", {
    method: "POST",
    headers: {
      "Content-Type": "application/json"
    },
    body: JSON.stringify(credentials)
  })
    .then(data => data.json());
}


const LoginForm = ({ setToken }) => {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");

  const [register, setRegister] = useState(false);

  //////////for server test
  const [data, setData] = useState();
  useEffect(() => {
    fetch("http://localhost:3003/api")
      .then((res) => res.json())
      .then((data) => setData(data.message));
  }, []);
  ////////

  const handleUser = async e => {
    e.preventDefault();

    if (register) {
      console.log("register");

      const data = {
        username: username,
        password: password
      };
      axios.post("http://localhost:3003/register", data);

    } else {
      const token = await loginUser({
        username,
        password
      });
      setToken(token);
    }
  };

  const handleButton = (e) => {
    e.preventDefault();

    register ? setRegister(false) : setRegister(true);
  };

  return (
    <form onSubmit={handleUser}
      style={{ textAlign: "center", marginTop: "30%", backgroundColor: "rgb(180, 192, 180)" }}>
      <Typography fontSize={20} fontWeight={"bold"} textAlign="center">
        {register ?
          "Please enter username and password for new user"
          : "Please enter username and password"}
      </Typography>

      <TextFieldInput type="text" name="Username" value={username}
        label="Username" onChange={({ target }) => setUsername(target.value)} />

      <TextFieldInput type="password" name="Password" value={password}
        label="Password" onChange={({ target }) => setPassword(target.value)} />

      <ButtonAll type="submit" text={register ? "register" : "login"} />

      <p /><Button onClick={handleButton} color="secondary" size="small">
        {register ? "login" : "register"}
      </Button>

      <p />{!data ? <p style={{ color: "red" }}>Loading...</p> : data}
    </form >
  );
};

LoginForm.propTypes = {
  setToken: PropTypes.func.isRequired
};

export { LoginForm };