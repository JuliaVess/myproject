import React from "react";
import axios from "axios";
import { Card, Box, Typography } from "@mui/material";
import ButtonAll from "./mui/ButtonAll";
import petAvatar from "../images/avatar1.jpg";
import AccordionDetail from "./mui/AccordionDetail";

const Pet = ({ pet, showEditButtons,
  index, setPets, pets, setShowPetsList, setPetIndex,
  setAllVaccinations, setAllPhotos }) => {
  const bgn = new Date(0);
  const petBD = new Date(pet.age);
  const day = new Date(Date.now() - petBD.getTime());
  const pAge = day.getFullYear() - bgn.getFullYear();
  const pMnt = day.getMonth() - bgn.getMonth();
  const pDay = day.getDate() - bgn.getDate();



  const removePet = (i) => {
    if (confirm("Remove the pet?")) {
      setPets([...pets.slice(0, i), ...pets.slice(i + 1)]);

      axios.delete(`http://localhost:3003/petsList/${i}`);
    }
  };

  const editPet = (i) => {
    setAllPhotos(pets[i].photos);
    setAllVaccinations(pets[i].vaccination);
    setPetIndex(i);
    setShowPetsList(false);
  };


  return (
    <>
      <Card variant="elevation" elevation={12}
        sx={{
          minHeight: 100,
          position: "relative",
          weight: "80%",
          backgroundColor: "rgb(180, 192, 180)",
          color: "rgb(75,76,79)",
          marginTop: "5px"
        }}>

        {showEditButtons ?
          <>
            <ButtonAll onClick={() => editPet(index)} text="edit pet info" value="ltl" />
            <ButtonAll onClick={() => removePet(index)} text="remove pet" value="ltl" />
          </>
          : null}

        <Box sx={{ paddingLeft: "75%", marginTop: "5px" }}>
          {pet.img ?
            <img src={pet.img} alt='avatar' />
            : <img src={petAvatar} alt='avatar'
              style={{ height: 55, widht: 40, borderRadius: 50 }} />}

          <Typography fontSize={20} fontWeight={"bold"}>
            {pet.name}
          </Typography>
        </Box>

        {/* <Typography fontSize={15}>
          {pet.type}
        </Typography> */}

        <Box sx={{ marginRight: "80px", marginTop: "-100px" }}>
          <Typography fontSize={17} variant="caption" sx={{ color: "rgb(75,76,79)" }}>
            {pAge} years {pMnt} months {pDay} days
          </Typography>
          <Typography fontSize={17} variant="h4" fontWeight={"300"}
            sx={{ color: "rgb(75,76,79)" }}>
            {pet.age}
          </Typography>
        </Box>

        {/* //panel, id, aria, summary, details */}


        <AccordionDetail panel="panel1" aria="panel1d-content" id="panel1d-header"
          summary="more info"
          details={
            <>{pet.weight ? <> weight: {pet.weight} kg ({pet.wDate})</> : <b>no info</b>}

              <p />feed: {pet.feed.feed} {pet.feed.feedWeight ?
                <>price: {pet.feed.feedWeight} kg for {pet.feed.feedPrice} euro
                  <p /><img src={pet.feed.feedPic} alt='feed' style={{ height: 60, widht: 60 }} />
                  <img src={pet.feedPic} style={{ weight: 100, height: 100 }} /></>
                : <b>no info</b>}

              <p />size: {pet.size.backLength || pet.size.neckGirth
                || pet.size.chestGirth ?
                <div>
                  <p>back length: {pet.size.backLength} cm</p>
                  <p>neck girth: {pet.size.neckGirth} cm</p>
                  <p>chest girth: {pet.size.chestGirth} cm</p>
                </div>
                : <b>no info</b>}

              vaccination: {pet.vaccination ?
                pet.vaccination.map(v =>
                  <p key={v.name}><b>{v.name}</b> {v.date} {v.nextDate}</p>)
                : <b>no info</b>}

              info: {pet.info ?
                <p>{pet.info}</p>
                : <b>no info</b>}

              photos: {pet.photos.map(p =>
                <img key={p.uuid} src={p.photos} style={{ weight: 100, height: 100 }} />)}
            </>
          } />

        {/* Fix the AccordionDetails height */}
        {/* <AccordionDetail panel="panel1" aria="panel1d-content" id="panel1d-header"
          summary="weight:"
          details={pet.weight ? <> weight: {pet.weight} kg ({pet.wDate})</> : <b>no info</b>} />

        <AccordionDetail panel="panel2" aria="panel2d-content" id="panel2d-header"
          summary="feed: {pet.feed.feed}"
          details={pet.feed.feedWeight ?
            <>price: {pet.feed.feedWeight} kg for {pet.feed.feedPrice} euro
              <p /><img src={pet.feed.feedPic} alt='feed' style={{ height: 60, widht: 60 }} />
              <img src={pet.feedPic} style={{ weight: 100, height: 100 }} /></>
            : <b>no info</b>} />

        <AccordionDetail panel="panel3" aria="panel3d-content" id="panel3d-header"
          summary="size:"
          details={pet.size.backLength || pet.size.neckGirth
            || pet.size.chestGirth ?
            <div>
              <p>back length: {pet.size.backLength} cm</p>
              <p>neck girth: {pet.size.neckGirth} cm</p>
              <p>chest girth: {pet.size.chestGirth} cm</p>
            </div>
            : <b>no info</b>} />

        <AccordionDetail panel="panel4" aria="panel4d-content" id="panel4d-header"
          summary="vaccination:"
          details={pet.vaccination ?
            pet.vaccination.map(v =>
              <p key={v.name}><b>{v.name}</b> {v.date} {v.nextDate}</p>)
            : <b>no info</b>} />

        <AccordionDetail panel="panel5" aria="panel5d-content" id="panel5d-header"
          summary="more info:"
          details={pet.info ?
            <p>{pet.info}</p>
            : <b>no info</b>} />

        <AccordionDetail panel="panel6" aria="panel6d-content" id="panel6d-header"
          summary="photos:"
          details={pet.photos.map(p =>
            <img key={p.uuid} src={p.photos} style={{ weight: 100, height: 100 }} />)} /> */}

      </Card>

    </>
  );
};

export { Pet };