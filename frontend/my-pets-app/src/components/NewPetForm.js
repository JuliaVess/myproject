import axios from "axios";
import React from "react";
import { useState } from "react";
import { AvatarUpload } from "./AvatarUpload";
import ButtonAll from "./mui/ButtonAll";
// import { uuid } from "uuidv4";

const NewPetForm = ({ pets, setPets, setShowPetsList,
  setShowEditButtons, petIndex, setPetIndex, allPhotos, setAllPhotos,
  allVaccinations, setAllVaccinations }) => {

  const today = new Date().toISOString().split("T")[0];//date for max input day(today)
  const [preview, setPreview] = useState(null);//to select an avatar

  const [photo, setPhoto] = useState();
  const [feedPc, setFeedPc] = useState();

  const [vaccination, setVaccination] = useState("");
  const [vaccinationDate, setVaccinationDate] = useState("");
  const [vaccinationNextDate, setVaccinationNextDate] = useState("");

  const today2 = new Date();
  const petsTypes = ["other", "dog", "cat", "monky", "snake", "mouse"];


  const addNewPet = (e) => {
    e.preventDefault();

    const newNameUpdate = e.target.petName.value[0].toUpperCase() + e.target.petName.value.slice(1);

    const newPet = {
      img: preview,
      name: newNameUpdate,
      age: e.target.bDayData.value,
      type: e.target.o.value,
      weight: e.target.petWeight.value,
      wDate: today2.toLocaleDateString("en-US"),
      feed: {
        feed: e.target.feed.value,
        feedWeight: e.target.feedWeight.value,
        feedPrice: e.target.feedPrice.value,
        feedPic: feedPc
      },
      size: {
        backLength: e.target.backLength.value,
        neckGirth: e.target.neckGirth.value,
        chestGirth: e.target.chestGirth.value
      },
      vaccination: allVaccinations,
      info: e.target.moreInfo.value,
      photos: allPhotos
    };


    setAllVaccinations([]);
    setVaccination("");
    setVaccinationDate("");
    setVaccinationNextDate("");
    setAllPhotos([]);
    setPreview(null);

    setShowPetsList(true);
    setShowEditButtons(false);

    if (petIndex === null) {
      setPets(pets.concat(newPet));//remove

      axios.post("http://localhost:3003/pet", newPet)
        .then(console.log("Pet is sended"))
        .catch((err) => { console.log(err); });

    } else {
      setPets([...pets.slice(0, petIndex), newPet, ...pets.slice(petIndex + 1)]);

      axios.put(`http://localhost:3003/petsList/${petIndex}`, newPet)
        .then(console.log("Pet is editing"));
    }



    const petToDB = {
      name: newNameUpdate,
      petType: e.target.o.value,
      birhday: e.target.bDayData.value,
      weight: e.target.petWeight.value,
      image: preview,
      backLength: e.target.backLength.value,
      neckGirth: e.target.neckGirth.value,
      chestGirth: e.target.chestGirth.value,
      feedName: e.target.feed.value,
      feedWeightPckg: e.target.feedWeight.value,
      feedPrice: e.target.feedPrice.value,
      feedImage: feedPc,
      moreInfo: "some info",
      userId: 8
      // vaccination: allVaccinations,
      // info: e.target.moreInfo.value,
      // photos: allPhotos
    };

    axios.post("http://localhost:3003/pets", petToDB)
      .then(console.log("New pet is sended to server and DB"))
      .catch((err) => {
        console.log(err);
      });


  };


  const addVaccine = (e) => {
    e.preventDefault();

    const newVaccine = {
      name: vaccination,
      date: " done date: " + vaccinationDate,
      nextDate: " next date: " + vaccinationNextDate
    };

    setAllVaccinations(allVaccinations.concat(newVaccine));
    setVaccination("");
    setVaccinationDate("");
    setVaccinationNextDate("");
  };


  const addMorePhoto = (e) => {
    e.preventDefault();

    const newPhoto = {
      photos: photo
    };
    setAllPhotos(allPhotos.concat(newPhoto));
    setPhoto();
  };

  const backButton = (e) => {
    e.preventDefault();
    setPetIndex(null);
    setAllVaccinations([]);
    setAllPhotos([]);
    setShowPetsList(true);
  };

  const removeVaccine = (i) => {
    console.log("remove vaccine", i);
    setAllVaccinations([...allVaccinations.slice(0, i), ...allVaccinations.slice(i + 1)]);
  };

  const removePhoto = (i) => {
    console.log("remove photo", i);
    setAllPhotos([...allPhotos.slice(0, i), ...allPhotos.slice(i + 1)]);
  };


  return (
    <div>
      <form onSubmit={addNewPet}>
        <AvatarUpload preview={petIndex === null ?
          preview : pets[petIndex].img} setPreview={setPreview} />

        <p />pet:
        <select name="o">
          {petsTypes.map((o) => (
            <option key={o} value={o}> {o}</option>
          ))}
        </select>

        <p />name*: <input type="text" required name='petName'
          disabled={petIndex === null ? false : true}
          defaultValue={petIndex === null ? "" : pets[petIndex].name} />
        <p />birthday*: <input type='date' name="bDayData" max={today} required
          disabled={petIndex === null ? false : true}
          defaultValue={petIndex === null ? null : pets[petIndex].age} />
        <p />weight: <input type='num' name="petWeight" placeholder="00.00"
          defaultValue={petIndex === null ? null : pets[petIndex].weight} /> kg
        <hr />
        <p />

        <u>pet size</u>
        <p />back length: <input type='num' name='backLength'
          defaultValue={petIndex === null ? null : pets[petIndex].size.backLength} /> cm
        <p />neck girth: <input type='num' name='neckGirth'
          defaultValue={petIndex === null ? null : pets[petIndex].size.neckGirth} /> cm
        <p />chest girth: <input type='num' name='chestGirth'
          defaultValue={petIndex === null ? null : pets[petIndex].size.chestGirth} /> cm
        <hr />
        <p />

        <u>feed</u>
        <p />feed name: <input type='text' name="feed"
          defaultValue={petIndex === null ? null : pets[petIndex].feed.feed} />
        <p />feed weight of packing: <input type='num' name="feedWeight"
          defaultValue={petIndex === null ? null : pets[petIndex].feed.feedWeight} /> kg
        <p />feed price: <input type='text' name="feedPrice"
          defaultValue={petIndex === null ? null : pets[petIndex].feed.feedPrice} /> euro
        <input type="file"
          onChange={(e) => setFeedPc(URL.createObjectURL(e.target.files[0]))} />
        {petIndex === null ? null : <img src={pets[petIndex].feedPic}
          style={{ weight: 100, height: 100 }} alt='feed' />}
        <hr />
        <p />

        <u>vaccination</u>
        <p>vaccination: <input type='text'
          value={vaccination} onChange={(e => setVaccination(e.target.value))} /></p>
        <p>data: <input type='date' max={today} value={vaccinationDate}
          onChange={(e => setVaccinationDate(e.target.value))} /></p>
        <p>date next vaccine: <input type='date' value={vaccinationNextDate}
          onChange={(e => setVaccinationNextDate(e.target.value))} /></p>

        <button onClick={addVaccine}>add vaccine</button>

        {allVaccinations ? allVaccinations.map((v =>
          <p key={v.name}><b>{v.name}</b> {v.date} {v.nextDate}
            <button onClick={() => removeVaccine(allVaccinations.indexOf(v))}>x</button>
          </p>))
          : null}
        <hr />

        <p />
        add info:
        <input type="text" className="textarea" name="moreInfo"
          style={{ height: 60, widht: 120 }}
          defaultValue={petIndex === null ? null : pets[petIndex].info} />
        <hr />

        <p />
        photo: <input type="file"
          onChange={(e) => setPhoto(URL.createObjectURL(e.target.files[0]))} />
        <button onClick={addMorePhoto}>add photo</button>
        <p />{allPhotos ? allPhotos.map(p =>
          <><img key={p.id} src={p.photos} style={{ weight: 100, height: 100 }} />
            <button onClick={() => removePhoto(allPhotos.indexOf(p))}>x</button></>)
          : null}
        <hr />

        <ButtonAll onClick={null} text={petIndex === null ? "save pet" : "seve edits"} />
      </form>
      <ButtonAll onClick={backButton} text="back" />
    </div>
  );
};

export { NewPetForm };