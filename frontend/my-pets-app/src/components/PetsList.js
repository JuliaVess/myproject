import React from "react";
import ButtonAll from "./mui/ButtonAll";
import { Pet } from "./Pet";

const PetsList = (props) => {
  return (
    <>
      {props.pets ? props.pets.map(p =>
        <Pet key={p.name} pet={p}
          index={props.pets.indexOf(p)}
          showEditButtons={props.showEditButtons}
          setShowEditButtons={props.setShowEditButtons}
          pets={props.pets} setPets={props.setPets}
          setShowPetsList={props.setShowPetsList}
          setPetIndex={props.setPetIndex}
          setAllVaccinations={props.setAllVaccinations}
          setAllPhotos={props.setAllPhotos}
        />) : null}

      {props.showEditButtons ?
        <ButtonAll onClick={() => props.setShowEditButtons(false)} text="back" />
        :
        <ButtonAll onClick={() => props.setShowPetsList(false)} text="add pet" />
      }

      {props.pets.length !== 0 ?
        props.showEditButtons ? null :
          <ButtonAll onClick={() => props.setShowEditButtons(true)} text="edit" />
        : null}
    </>
  );
};

export { PetsList };