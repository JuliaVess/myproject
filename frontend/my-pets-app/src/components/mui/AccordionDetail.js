import React from "react";
import { Typography, Accordion, AccordionDetails, AccordionSummary } from "@mui/material";


const AccordionDetail = ({ panel, id, aria, summary, details }) => {


  const [expanded, setExpanded] = React.useState(false);

  const style = {
    width: "100%",
    minHeight: "50px",
    backgroundColor: "rgb(180, 192, 180)",
    color: "rgb(75,76,79)"
  };


  const handleChange = (panel) => (e, newExpanded) => {
    setExpanded(newExpanded ? panel : false);
  };

  return (
    <>
      <Accordion expanded={expanded === panel} onChange={handleChange(panel)} sx={style} >
        <AccordionSummary aria-controls={aria} id={id}>
          <Typography sx={{ width: "23%", flexShrink: 0 }}>
            {summary}
          </Typography>
        </AccordionSummary>

        <AccordionDetails >
          <Typography>
            {details}
          </Typography>
        </AccordionDetails>
      </Accordion>
    </>
  );
};

export default AccordionDetail;