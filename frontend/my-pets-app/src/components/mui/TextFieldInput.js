import React from "react";
import { TextField } from "@mui/material";

const TextFieldInput = ({ type, name, value, label, onChange }) => {

  return (

    <TextField
      fullWidth
      variant="outlined"
      type={type}
      name={name}
      defaultValue={value}
      label={label}
      onChange={onChange}
      required
      //   size="small"
      sx={{
        // width: "40%",
        // height: "20",
        width: "15ch",
        margin: "5px",
        "& label.Mui-focused": {
          color: "gray",
        },
        // "& .MuiInput-underline:after": {
        //   borderBottomColor: "white",
        // },
        // "& .MuiInputLabel-root": {
        //   color: "white",
        // },
        "& .MuiOutlinedInput-root": {
          "& fieldset": {
            borderColor: "white",
            //   backgroundColor: "#ebe6eb",
          // height: "40px",
            //   border: "none"
          },
          "&:hover fieldset": {
            borderColor: "white",
          },
          "&.Mui-focused fieldset": {
            borderColor: "white",
          } }
      }}

    />

  );
};
export default TextFieldInput;