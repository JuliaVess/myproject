import React from "react";
import { Button } from "@mui/material";

const ButtonAll = ({ text, onClick, type, value }) => {

  const buttonStyleBig = {
    backgroundColor: "#c0b4c0",
    boxShadow: "2px 2px 7px 1px #79828b",
    border: "1px",
    borderRadius: "3px",
    margin: "5px",
    color: "#47494a",
    width: "60%",
    marginTop: "10px",
    ":hover": {
      backgroundColor: "#c1cfd6"
    }
  };

  const buttonStyleLittle = {
    backgroundColor: "#c0b4c0",
    boxShadow: "2px 2px 7px 1px #79828b",
    border: "1px",
    borderRadius: "3px",
    margin: "5px",
    color: "#47494a",
    width: "40%",
    marginTop: "5px",
    ":hover": {
      backgroundColor: "#c1cfd6"
    }
  };

  return (
    <Button onClick={onClick} sx={!value ? buttonStyleBig : buttonStyleLittle}
      type={type ? null : "submit"} size="small">
      {text}
    </Button>
  );
};
export default ButtonAll;