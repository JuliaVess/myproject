import React from "react";
import Avatar from "react-avatar-edit";

const AvatarUpload = ({ preview, setPreview }) => {

  const onBeforeFileLoad = (elem) => {
    if (elem.target.files[0].size > 2000000){
      alert("File is too big!");
      elem.target.value = "";
    }
  };

  return (
    <div>
      <Avatar width={200} height={80}
        onCrop={(e) => setPreview(e)}
        onClose={() => setPreview(null)}
        onBeforeFileLoad={onBeforeFileLoad}
        src={null}/>
      <br/>
      {preview && (
        <>
          <img src={preview} alt='Preview' />
          {/* <a href={preview} download='avatar' >
                Download avatar
          </a> */}
        </>
      )}
    </div>
  );
};

export { AvatarUpload };