import React from "react";
import { useState, useEffect } from "react";
import { Typography } from "@mui/material";
import { NewPetForm } from "./NewPetForm";
import { PetsList } from "./PetsList";

const LoggedIn = () => {
  const [pets, setPets] = useState([]);
  const [petIndex, setPetIndex] = useState(null);

  const [showPetsList, setShowPetsList] = useState(true);//for petlist and add button
  const [showEditButtons, setShowEditButtons] = useState(false);//for pet form edit and remove btns

  const [allPhotos, setAllPhotos] = useState([]);
  const [allVaccinations, setAllVaccinations] = useState([]);

  //
  const [petsList, setPetsList] = useState(false); //1
  useEffect(() => {
    getPets();
  }, [petsList]);

  const getPets = () => {
    fetch("http://localhost:3003/pets")
      .then(res => {
        return res.text();
      })
      .then(data => {
        setPetsList(data);
      })
      .then(console.log(petsList, "petsList from database"))
      .then(console.log(petsList[7], "test pet from server"));
  };
  //

  useEffect(() => {
    fetch("http://localhost:3003/petsList")
      .then((res) => res.json())
      .then((data) => setPets(data))
      .then(console.log(pets, "data pets from local backend"));
  }, []);


  return (
    <>
      {/* {petsList ? petsList.map(p => <p key={p.id}>{p.name}</p>) : null} */}
      <p />{petsList ? petsList[0].name : "pets not found"}

      <Typography fontSize={40} fontWeight={"bold"}
        sx={{ marginTop: "50px", marginBottom: "20px" }}>
        MY PETS
      </Typography>


      {showPetsList ?
        <PetsList
          pets={pets} setPets={setPets}
          showEditButtons={showEditButtons}
          setShowEditButtons={setShowEditButtons}
          setShowPetsList={setShowPetsList}
          petIndex={petIndex} setPetIndex={setPetIndex}
          setAllPhotos={setAllPhotos}
          setAllVaccinations={setAllVaccinations}
        />
        :
        <NewPetForm
          pets={pets} setPets={setPets}
          setShowPetsList={setShowPetsList}
          setShowEditButtons={setShowEditButtons}
          petIndex={petIndex} setPetIndex={setPetIndex}
          allVaccinations={allVaccinations} setAllVaccinations={setAllVaccinations}
          allPhotos={allPhotos} setAllPhotos={setAllPhotos}
        />
      }
    </>
  );
};

export { LoggedIn };